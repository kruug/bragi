#import asyncio
import discord
import os
import time

from discord.ext import commands
from dotenv import load_dotenv

startup_extensions = ["fng", "haiku", "glab", "marko"]

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

bot = commands.Bot(command_prefix='!')

@bot.listen()
async def on_ready():
    channel = bot.get_channel(683009876997439541)
    await channel.send('Carrier Online')

@bot.command(name='restart')
async def restart(ctx):
    await ctx.send("Restarting...")
    os.system("sudo service bragi restart")

@bot.command(name='load')
async def load(ctx, extension_name : str):
    #Loads an extension.
    try:
        bot.load_extension(extension_name)
    except (AttributeError, ImportError) as e:
        await ctx.send("```py\n{}: {}\n```".format(type(e).__name__, str(e)))
        return
    await ctx.send("{} loaded.".format(extension_name))

@bot.command(name='unload')
async def unload(ctx, extension_name : str):
    bot.unload_extension(extension_name)
    await ctx.send("{} unloaded.".format(extension_name))

@bot.command(name='reload')
async def reload(ctx, extension_name : str):
    bot.reload_extension(extension_name)
    await ctx.send("{} reloaded.".format(extension_name))

if __name__ == "__main__":
    for extension in startup_extensions:
        try:
#            asyncio.run(load(extension))
            bot.load_extension(extension)
            print(extension + " has loaded")
        except Exception as e:
            exc = '{}: {}'.format(type(e).__name__, e)
            print('Failed to load extension {}\n{}'.format(extension, exc))

    bot.run(TOKEN)
