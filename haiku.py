import spacy
import re
import markovify
import warnings
import discord
import base64
import requests
import syllapy
import random

from discord.ext import commands
from spacy.matcher import Matcher

warnings.filterwarnings('ignore')

nlp = spacy.load('en_core_web_sm')

class haiku(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='haiku')
    async def haiku(self, ctx):
        with open("/home/kruug/bragi/haiku.src") as f:
            texts = f.read()

        # Groups of 2 words
        matcher2w = Matcher(nlp.vocab)
        pattern = [{"POS":{"IN": ["NOUN", "ADV", "ADJ", "ADP"]}},{"POS":{"IN": ["VERB", "NOUN"]}}]
        matcher2w.add("2words", [pattern])

        # Groups of 3 words
        matcher3w = Matcher(nlp.vocab)
        pattern = [{"POS":{"IN": ["NOUN", "ADV", "ADJ", "VERB", "ADP"]}},{"IS_ASCII": True, "IS_PUNCT": False},{"POS":{"IN": ["VERB", "NOUN", "ADV", "ADJ"]}}]
        matcher3w.add("3words", [pattern])

        # Groups of 4 words
        matcher4w = Matcher(nlp.vocab)
        pattern = [{"POS":{"IN": ["NOUN", "ADV", "ADJ", "VERB", "ADP"]}},{"IS_ASCII": True, "IS_PUNCT": False},{"IS_ASCII": True, "IS_PUNCT": False},{"POS":{"IN": ["VERB", "NOUN", "ADV", "ADJ"]}}]
        matcher4w.add("4words", [pattern])

        # Identify patterns in the text
        doc = nlp(texts)
        matches2w = matcher2w(doc)
        matches3w = matcher3w(doc)
        matches4w = matcher4w(doc)

#        print("Generating haiku")

        lines_5_syll = []
        lines_7_syll = []

        for match_id, start, end in matches2w + matches3w + matches4w:
            string_id = nlp.vocab.strings[match_id]
            span = doc[start:end]

            syllable_count = 0

            for token in span:
                syllable_count += syllapy.count(token.text)

            if syllable_count == 5:
                if span.text not in lines_5_syll:
                    lines_5_syll.append(span.text)
            if syllable_count == 7:
                if span.text not in lines_7_syll:
                    lines_7_syll.append(span.text)

        response = ("{0}\n{1}\n{2}".format(random.choice(lines_5_syll), random.choice(lines_7_syll), random.choice(lines_5_syll)))
        await ctx.send(response)

def setup(bot):
    bot.add_cog(haiku(bot))
