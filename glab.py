import discord
import gitlab
import os
import warnings

from datetime import datetime
from discord.ext import commands
from dotenv import load_dotenv

warnings.filterwarnings('ignore')

class glab(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='issue')
    async def issue(self, ctx, *suggestion):
        load_dotenv()
        TOKEN = os.getenv('GITLAB_TOKEN')

        suggest = ''
        for word in suggestion:
            suggest = suggest + word
            suggest = suggest + ' '
        
        today = datetime.today().strftime('%Y-%m-%d')
        full_title = 'Suggestion from Discord ' + today

        gl = gitlab.Gitlab('https://gitlab.com', private_token=TOKEN)
        project = gl.projects.get(26484208)

        issue = project.issues.create({'title': full_title, 'description': suggest})
        ititle = issue.attributes["title"]
        idesc = issue.attributes["description"]
        iurl = issue.attributes["web_url"]

        embed = discord.Embed(title=ititle, url=iurl, description=idesc, color=0xd9d904)

        await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(glab(bot))
