import spacy
import re
import markovify
import warnings
import discord

from discord.ext import commands

warnings.filterwarnings('ignore')

nlp = spacy.load('en_core_web_sm')

#utility function for text cleaning
def text_cleaner(text):
    text = re.sub(r'--', ' ', text)
    text = re.sub(r'_', ' ', text)
    text = re.sub('[\[].*?[\]]', '', text)
    text = re.sub(r'(\b|\s+\-?|^\-?)(\d+|\d*\.\d+)\b','', text)
    text = ' '.join(text.split())
    return text

class POSifiedText(markovify.Text):
  def word_split(self, sentence):
    return ['::'.join((word.orth_, word.pos_)) for word in nlp(sentence)]

  def word_join(self, words):
    sentence = ' '.join(word.split('::')[0] for word in words)
    return sentence

class Marko(commands.Cog):
  def __init__(self, bot):
    self.bot = bot
    
    corpus = ''
    
    with open("/home/kruug/bragi/split/marko_aa") as f:
      text = f.read()

    clean_text = text_cleaner(text)

    corpus = nlp(clean_text)
    corpus_sents = ' '.join([sent.text for sent in corpus.sents if len(sent.text) > 1])

    #generator_1 = markovify.Text(corpus_sents, state_size=3)

    #print(generator_1.make_sentence())#We will randomly generate three more sentences of no more than 100 characters
    #print(generator_1.make_short_sentence(max_chars=100))

    self.generator_2 = POSifiedText(corpus_sents, state_size=3)

  @commands.command(name='marko')
  async def marko(self, ctx):
      print("generating text")
      response = (self.generator_2.make_sentence()) #print 100 characters or less sentences
      await ctx.send(response)
  #print(generator_2.make_short_sentence(max_chars=100))

def setup(bot):
    bot.add_cog(Marko(bot))
