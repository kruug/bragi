import warnings
import discord
import base64
import pytz
import random

from datetime import datetime
from discord.ext import commands
from time import sleep

warnings.filterwarnings('ignore')

class fng(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='fng')
    async def fng(self, ctx):
        await ctx.send("Hello")

    @commands.command(name='choose', description='For when you want to settle the score some other way')
    async def choose(self, ctx, *choices : str):
        choices_join = " ".join(choices)
        options = choices_join.split(",")
        await ctx.send(random.choice(options))

    @commands.command(name='ping')
    async def ping(self, ctx):
        await ctx.send('pong')

    @commands.command(name='pong')
    async def pong(self, ctx):
        await ctx.send('ping')

    @commands.command(name='bingo')
    async def bingo(self, ctx):
        await ctx.send('https://i.imgur.com/fypAmty.mp4')

    @commands.command(name='bongo')
    async def bongo(self, ctx):
        await ctx.send('https://i.imgur.com/Mc8hPgr.mp4')

    @commands.command(name='oingo')
    async def oingo(self, ctx):
        await ctx.send('https://www.youtube.com/watch?v=H2LQMElLoLs')

    @commands.command(name='annoy')
    async def annoy(self, ctx, user : discord.User):
        for x in range (0,11):
            await ctx.send(user.mention)
            sleep(2)

    @commands.command(name='time')
    async def time(self, ctx):
        UTC = pytz.utc
        CST = pytz.timezone('US/Central')

        time_UTC = datetime.now(UTC)
        time_CST = datetime.now(CST)

        ftime_UTC = time_UTC.strftime('%H:%M:%S')
        ftime_CST = time_CST.strftime('%H:%M:%S')

        describe = f'UTC: {ftime_UTC}\nUS Central: {ftime_CST}'

        time_embed = discord.Embed(title='Current Time', url='', description=describe, color=discord.Color.dark_gray())
        time_embed.set_thumbnail(url='https://i.imgur.com/NT4qIA1.png')

#        await ctx.send(datetime.now().strftime('%H:%M:%S'))
        await ctx.send(embed=time_embed)

def setup(bot):
    bot.add_cog(fng(bot))
